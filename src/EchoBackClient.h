/*
 * EchoBackClient.h
 *
 *  Created on: 2016/03/08
 *      Author: yasuh
 */

#ifndef ECHOBACKCLIENT_H_
#define ECHOBACKCLIENT_H_

#include <string>

class EchoBackClient {

public:
	EchoBackClient();
	virtual ~EchoBackClient();

	void start( std::string host, int port );
};

#endif /* ECHOBACKCLIENT_H_ */
