/*
 * EchoBackClient.cpp
 *
 *  Created on: 2016/03/08
 *      Author: yasuh
 */
#include "EchoBackClient.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/FIFOBuffer.h"
#include <iostream>

using namespace Poco;
using namespace Poco::Net;
using namespace std;

EchoBackClient::EchoBackClient() {
}

EchoBackClient::~EchoBackClient() {
}

void EchoBackClient::start(std::string host, int port) {

	try {
		StreamSocket socket;
		SocketAddress address(host, port);
		socket.connect(address);
		string user_input;
		char recv_message[256];
		while (true) {

			// console input
			getline(cin, user_input);
			string::iterator last = remove_if(user_input.begin(), user_input.end(),
					[](char ch) {return ch == '\r' || ch == '\n';});
			user_input.erase(last, user_input.end());

			// send message to the server
			socket.sendBytes(user_input.c_str(), user_input.size());

			// receive echo back from the server after 1 sec
			sleep(1);
			uint32_t size = socket.receiveBytes(recv_message,
					sizeof(recv_message));
			recv_message[size] = '\0';

			// console out
			cout << recv_message;
			cout.flush();
		}
		socket.close();
	} catch (const Exception & e) {
		cerr << e.what() << endl;
	}
}
