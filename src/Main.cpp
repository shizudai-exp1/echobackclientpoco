//============================================================================
// Name        : Main.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : EchoBackClient
//============================================================================
#include "EchoBackClient.h"

int main(int argc, char** argv) {
	EchoBackClient ebc;
	ebc.start("localhost", 8000);
	return 0;
}
